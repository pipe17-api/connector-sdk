function toIterator(o) {
  if (!o) {
    return [][Symbol.iterator]();
  } else if (typeof o === 'string') {
    return [o][Symbol.iterator]();
  } else if (Symbol.iterator in o) {
    return o[Symbol.iterator]();
  } else if (Symbol.asyncIterator in o) {
    return o[Symbol.asyncIterator]()
  } else {
    return [o][Symbol.iterator]();
  }
}

/**
 * @callback fetch
 * @param {...any} args
 * @returns {any|any[]|Promise<any>|Promise<any[]>}
 */

/**
 * @callback apply
 * @param {any} item
 * @param {...any} args
 * @returns {any|Promise<any>}
 */

/**
 * @param {Object} task 
 * @param {boolean} task.concurrent 
 * @param {fetch} task.fetch 
 * @param {apply} task.apply 
 * @param {apply} task.done 
 * @param {*} args 
 * @returns {Promise<any[]>}
 */
async function exec(task, query, ...args) {
  const result = [];

  const fetched = await task.fetch(query, ...args);

  const it = toIterator(fetched);
  let last = null;
  let index = 0;
  let item = await it.next();
  while (!item.done) {
    
    last = item.value;
    const dfd = task.apply(last, index++).catch((error) => {
      return { error };
    });

    const applied = task.concurrent ? dfd : await dfd;
    result.push(applied);

    // we can report applied value back if this is a true iterator
    // iterator can adjust behavior as necessary
    item = await it.next(applied);
    if (applied && applied.error && !applied.error.acknowledged) {
      throw applied.error;
    }
  }

  await task.done(last, index - 1, ...args);
  return task.concurrent ? Promise.all(result) : result;
}

module.exports = {
  exec
}