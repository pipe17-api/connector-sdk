const api = require('@pipe17/api-client');
const { exec } = require('../runtime/exec');
const { TriggerTypes, TriggerType } = require('./triggers');
TriggerType;
const logger = require('../common/logger').defaultLogger.child({ module: 'p17-meta' });

/**
 * @typedef {import('../context').Context} Context
 */

/**
 * @typedef {function(Context, Object, ...any): any | any[] | Promise<any> | Promise<any[]>} itemsHandler 
 */
/**
 * @typedef {function(Context, string, ...any): any | any[] | Promise<any> | Promise<any[]>} fetchHandler 
 */
/**
 * @typedef {function(Context, Object, number, ...any): any | any[] | Promise<any> | Promise<any[]>} applyHandler 
 */
/**
 * @typedef {function(Context, Object, number, ...any): string | Date | Promise<string> | Promise<Date>} getTimestampHandler
 */
/**
 * @typedef {function(Context, string, number, string, ...any): Promise} setTimestampHandler
 */
/**
 * @typedef {function(Context, Object, string, string, ...any): Promise<Object>} lastTimestampHandler
 */
/**
 * @typedef {function(Context, Object, string, string, ...any): Promise<Object>} getQueryHandler
 */

/**
 * @typedef {Object} VendorHandlers
 * @prop {itemsHandler} items
 * @prop {fetchHandler} fetch
 * @prop {applyHandler} apply
 * @prop {getTimestampHandler} getTimestamp
 */

/**
 * @typedef {Object} P17Handlers
 * @prop {itemsHandler} items
 * @prop {boolean} itemsDetails
 * @prop {fetchHandler} fetch
 * @prop {applyHandler} apply
 * @prop {getTimestampHandler} getTimestamp
 * @prop {setTimestampHandler} setTimestamp
 * @prop {lastTimestampHandler} lastTimestamp
 * @prop {getQueryHandler} getQuery
 */


/**
 * @class
 */
class EntityHandler {
  /**
   * @param {api.EntityName|'integrations'} entityType 
   */
  constructor(entityType) {
    logger.info('entityHandler:ctor', { entityType });
    /** @private */
    this._type = entityType;

    /** @type {Object.<string,TriggerType>} */
    /** @private */
    this._triggers = {};

    /** @type {P17Handlers} */
    /** @private */
    this._p17_handlers = {}
    /** @type {VendorHandlers} */
    /** @private */
    this._vendor_handlers = {};

    this.defaultP17Handlers();
  }
  
  /**
   * @private 
   * @param {import('./triggers').TriggerType} triggerType
   */
  getHandlers(triggerType) {
    const trigger = this._triggers[triggerType.name];
    if (!trigger) {
      return null;
    }
    
    if (triggerType === TriggerTypes.VendorPoll) {
      return {
        done: async (context, item, index, ...args) => {
          await this._p17_handlers.setTimestamp(context, item, index, 'in', true, ...args);
        },
        fetch: async (context, query, ...args) => {
          query = query || {};
          const timestamp = await this._p17_handlers.lastTimestamp(context, query, this._type, 'in', ...args);
          if (timestamp) {
            query.since = timestamp;
          }
          if (this._vendor_handlers.getQuery){
            query = await this._vendor_handlers.getQuery(context, query, this._type, 'in', ...args);
          }
          if (!query) {
            return [];
          }
          return await this._vendor_handlers.items(context, query, ...args);
        },
        apply: async (context, item, index, ...args) => {
          const converted = await context.connector.convert(context, item, this._type, 'in');
          const result = await this._p17_handlers.apply(context, converted, index, ...args);
          await this._p17_handlers.setTimestamp(context, item, index, 'in', false, ...args);
          return result;
        },
      }
    }
    if (triggerType === TriggerTypes.P17Poll) {
      return {
        done: async (context, item, index, ...args) => {
          await this._p17_handlers.setTimestamp(context, item, index, 'out', true, ...args);
        },
        fetch: async (context, query, ...args) => {
          query = query || {};
          const timestamp = await this._p17_handlers.lastTimestamp(context, query, this._type, 'out', ...args);
          if (timestamp) {
            query.updatedSince = timestamp;
          }
          if (this._p17_handlers.getQuery){
            query = await this._p17_handlers.getQuery(context, query, this._type, 'out', ...args);
          }
          if (!query) {
            return [];
          }
          
          return await this._p17_handlers.items(context, query, ...args);
        },
        apply: async (context, item, index, ...args) => {
          let fetched = item;
          if (this._p17_handlers.itemsDetails) {
            fetched = await this._p17_handlers.fetch(context, item, ...args).catch((error) => {
              // need logger here
              // return original item
              return item;
            })
          }
          const converted = await context.connector.convert(context, fetched, this._type, 'out');
          const result = await this._vendor_handlers.apply(context, converted, index, ...args);
          await this._p17_handlers.setTimestamp(context, item, index, 'out', false, ...args);
          return result;
        },
      }
    }
    if (triggerType === TriggerTypes.VendorEvent) {
      return {
        done: async (context, item, index, ...args) => {
          context, item, index, args;
        },
        fetch: async (context, query, ...args) => {
          return await this._vendor_handlers.items(context, query, ...args);
        },
        apply: async (context, item, index, ...args) => {          
          let converted = await context.connector.convert(context, item, this._type, 'in');
          return await this._p17_handlers.apply(context, converted, index, ...args);
        },
      }
    }
    if (triggerType === TriggerTypes.P17Event) {
      return {
        done: async (context, item, index, ...args) => {
          context, item, index, args;
        },
        fetch: async (context, id, ...args) => {
          return await this._p17_handlers.fetch(context, id, ...args);
        },
        apply: async (context, item, index, ...args) => {
          let converted = await context.connector.convert(context, item, this._type, 'out');
          return await this._vendor_handlers.apply(context, converted, index, ...args);
        },
      }
    }
  }

  /**
   * @param {import('./triggers').TriggerType} type
   */
  withTrigger(type) {
    this._triggers[type.name] = type;
    return this;
  }

  /**
   * @param {VendorHandlers} vendor 
   * @param {P17Handlers} [p17={}}
   */
  withHandlers(vendor, p17) {
    this._vendor_handlers = { ...this._vendor_handlers, ...vendor};
    this._p17_handlers = { ...this._p17_handlers, ...p17};
    return this;
  }
  /** @private */
  defaultP17Handlers() {
    this._p17_handlers = {
      apply: async(context, item, index, ...args) => {
        index, args;
        const api = context.api;
        return await api[this._type].upsert(item)
      },
      fetch: async(context, id, ...args) => {
        args;
        const api = context.api;
        return await api[this._type].fetch(id)
      },
      items: async (context, query, ...args) => {
        args;
        const api = context.api;
        return await api[this._type].iterator(query)
      },
      lastTimestamp: async (context, query, type, direction, ...args) => {
        args;
        return await context.connector.lastTimestamp(context, query, type, direction);
      },
      getTimestamp: async(context, item, index, ...args) => {
        context, index, args;
        return (item || {}).updatedAt;
      },
      setTimestamp: async(context, item, index, direction, force, ...args) => {
        let handler = null;
        if (direction === 'in') {
          handler = this._vendor_handlers.getTimestamp;
        } else {
          handler = this._p17_handlers.getTimestamp;
        }
        if (!handler) {
          return;
        }
        await handler(context, item, index, ...args).then((timestamp) => {
          return context.connector.setTimestamp(context, this._type, direction, index, force, timestamp);;
        }).catch((error) => {
          error;
        });
      },
      // getQuery: async (context, query, type, direction, ...args) => {
      //   return query;
      // },
    }
  }
}

class EntityHandlers {
  constructor() {
    /** @type {Object.<api.EntityName, EntityHandler>} */
    /** @private */
    this._entities = { };

    this.integrations.withTrigger(TriggerTypes.P17Poll).withHandlers({
      apply: async (context, item, index, ...args) => {
        index, args;
        return await context.connector.exec({ ...context.args, integrationId: item.integrationId })
      }
    });
  }
  /**
   * @private
   * @param {api.EntityName|'integrations'} type 
   * @returns {EntityHandler}
   */
  _getEntityHandler(type) {
    return this._entities[type] || (this._entities[type] = new EntityHandler(type));
  }
  get arrivals() {
    return this._getEntityHandler(api.EntityName.Arrivals);
  }
  get locations() {
    return this._getEntityHandler(api.EntityName.Locations);
  }
  get fulfillments() {
    return this._getEntityHandler(api.EntityName.Fulfillments);
  }
  get inventory() {
    return this._getEntityHandler(api.EntityName.Inventory);
  }
  get orders() {
    return this._getEntityHandler(api.EntityName.Orders);
  }
  get products() {
    return this._getEntityHandler(api.EntityName.Products);
  }
  get purchases() {
    return this._getEntityHandler(api.EntityName.Purchases);
  }
  get receipts() {
    return this._getEntityHandler(api.EntityName.Receipts);
  }
  get refunds() {
    return this._getEntityHandler(api.EntityName.Refunds);
  }
  get returns() {
    return this._getEntityHandler(api.EntityName.Returns);
  }
  get shipments() {
    return this._getEntityHandler(api.EntityName.Shipments);
  }
  get transfers() {
    return this._getEntityHandler(api.EntityName.Transfers);
  }
  get integrations() {
    return this._getEntityHandler('integrations');
  }
  get webhooks() {
    return this._getEntityHandler('webhooks');
  }
  /**
   * @private
   * @param {Context} context 
   */
  getActions(context) {
    /** @type {TriggerType[]} */
    const triggerTypes = Array.isArray(context.triggerTypes) ? context.triggerTypes : [context.triggerTypes];
    /** @type {api.EntityType[]} */
    const entityTypes = !context.entityTypes ? undefined : Array.isArray(context.entityTypes) ? context.entityTypes : [context.entityTypes];
  
    const filtered = Object.getOwnPropertyNames(this._entities).filter((x) => (!entityTypes && (x !== 'integrations')) || (entityTypes &&  entityTypes.includes(x)));
    const result = triggerTypes.reduce((r, triggerType) => {
      const filtered2 = filtered.filter((x) => {
        if (x === 'integrations') {
          return true;
        }
        const found = context.configuration.mappings.mappings.find((m) => {
          const direction = (triggerType === TriggerTypes.P17Poll || triggerType === TriggerTypes.P17Event) ? api.Direction.Out : api.Direction.In;
          return (m.name === x) && (m.direction === direction);
        });
        return !!found;
      });
      const handlers = filtered2.map((entityType) => {
        /** @type {EntityHandler} */
        const entity = this._entities[entityType];
        return { entityType, handlers: entity.getHandlers(triggerType) };
      })
      r.push(...handlers);
      return r;
    }, []).sort((a,b) => {
      const et = context.entityTypes || [];
      return et.indexOf(a.entityType) - et.indexOf(b.entityType);
    }).map((x) => {
      return x.handlers;
    }).filter((handler) => {
      return handler && (handler.fetch && handler.apply);
    }).map((handler) => {
      return async (...args) => {
        return await exec({ 
          concurrent: context.concurrent, 
          fetch: async(query) => { return await handler.fetch(context, query, ...args) }, 
          apply: async(item, index, ...args) => { return await handler.apply(context, item, index, ...args) },
          done: async(item, index, ...args) => { return await handler.done(context, item, index, ...args) } 
        });
      } 
    }).map((exec) => {
      return { exec: exec };
    });
  
    return result;
  }
}


module.exports = {
  EntityHandlers
}