const logger = require('../common/logger').defaultLogger.child({ module: 'p17-meta' });

const { API, ValueType } = require('../api');
const { Connector } = require('../connector');
const { Configuration } = require('../configuration');
const { TriggerTypes } = require('./triggers');
const { ConnectorStatus, IntegrationStatus } = require('@pipe17/api-client');

class ConnectorMeta {
  constructor() {
    logger.info('ctor');
    this.configuration = new Configuration();
  }

  /**
   * @returns {import('./entities').EntityHandlers}
   */
  get entities() {
    return this.configuration.entities;
  }
  /**
   * @param {string} name 
   */
  withName(name) {
    this.configuration.name = name;
    return this;
  }
  /**
   * @param {string} description 
   */
  withDescription(description) {
    this.configuration.description = description;
    return this;
  }
  /**
   * @param {string} readme 
   */
  withReadme(readme) {
    this.configuration.readme = readme;
    return this;
  }
  /**
   * @param {import('../api').ConnectorType} type 
   */
  withType(type) {
    this.configuration.type = type;
    return this;
  }
  /**
   * @param {string} logoUrl 
   */
  withLogoUrl(logoUrl) {
    this.configuration.logoUrl = logoUrl;
    return this;
  }
  /**
   * @param {string} version 
   */
  withVersion(version) {
    this.configuration.version = version;
    return this;
  }
  /**
   * @param {string} version 
   */
   withOrgWideAccess(enabled) {
    this.configuration.orgWideAccess = enabled;
    return this;
  }
  /**
   * @param {Object} configuration 
   * @param {string} configuration.basePath 
   * @param {string} configuration.apiKey 
   */
  withAPIConfiguration(configuration) {
    this.configuration.api_configuration.basePath = configuration.basePath;
    this.configuration.api_configuration.apiKey = configuration.apiKey;
    return this;
  }
  /**
   * @param {Object} configuration 
   * @param {string} configuration.basePath 
   * @param {string} configuration.apiKey 
   */
  withSchemaAPIConfiguration(configuration) {
    this.configuration.schema_api_configuration.basePath = configuration.basePath;
    this.configuration.schema_api_configuration.apiKey = configuration.apiKey;
    return this;
  }


  /**
   * @param {string} url 
   * @param {import('@pipe17/api-client').ConnectorConnectionTypeEnum} type
   */
  withConnection(url, type) {
    this.configuration.connectionSettings.url  = url;
    this.configuration.connectionSettings.type = type;
    return this;
  }
  /**
   * @param {Object} setting 
   * @param {string} setting.path
   * @param {ValueType} [setting.type='string']
   * @param {boolean} [setting.secret]
   * @param {import('@pipe17/api-client').ConnectorSettingsOptions} [setting.options]
   */
  withConnectionSetting(setting) {
    this.configuration.connectionSettings.withSetting(setting.path, setting.type, setting.secret, setting.options);
    return this;
  }

  /**
   * @param {Object} setting 
   * @param {string} setting.path
   * @param {ValueType} [setting.type='string']
   * @param {boolean} [setting.secret]
   * @param {import('@pipe17/api-client').ConnectorSettingsOptions} [setting.options]
   */
  withSetting(setting) {
    this.configuration.settings.withSetting(setting.path, setting.type, setting.secret, setting.options);
    return this;
  }
  /**
   * @param {string} url 
   * @param {string} apiKey 
   */
  withWebHook(url, apiKey) {
    this.configuration.webhook.url = url;
    this.configuration.webhook.apiKey = apiKey;
    return this;
  }
  /**
   * @param {Object} mapping 
   * @param {import('../api').EntityType} mapping.entityType 
   * @param {import('../api').Direction} mapping.direction 
   * @param {string} mapping.mappingId
   * @param {string} mapping.routingId
   */
  withMapping(mapping) {
    this.configuration.mappings.withMapping(mapping.entityType, mapping.direction, mapping.mappingId);
    return this;
  }

  build() {
    return new Connector(this.configuration);
  }

  /**
   * @param {string} mappingsLocation
   */
  async register(mappingsLocation) {
    const name = this.configuration.name;
    logger.info('connector:register', { name: name });

    const api = new API({ api: this.configuration.api_configuration });

    this.configuration.mappings.loadMappings(mappingsLocation);
    const mappings = await api.mappings.updateBatch(this.configuration.mappings.mappings, false, this.configuration.name);

    const connectors = await api.connectors.list(this.configuration.name);

    const topics = Object.getOwnPropertyNames(this.entities._entities).filter(x => {
      const entity = this.entities._entities[x];
      return !!entity._triggers[TriggerTypes.P17Event.name];
    });
  
    /** @type {import('@pipe17/api-client').Connector} */
    let connector = null;
    if (connectors.length === 0) {
      /** @type {import('@pipe17/api-client').ConnectorCreateRequest} */
      const payload = {
        connectorType: this.configuration.type,
        connectorName: this.configuration.name,
        version: this.configuration.version,
        description: this.configuration.description,
        readme: this.configuration.readme,
        orgWideAccess: this.configuration.orgWideAccess,
        logoUrl: this.configuration.logoUrl,
        connection: this.configuration.connectionSettings.clone(),
        settings: this.configuration.settings.clone(),
        webhook: this.configuration.webhook.url && topics.length ? { ...this.configuration.webhook, ...{ apikey: this.configuration.webhook.apiKey || "" }, topics } : undefined,
        entities: mappings
      };
      connector = await api.connectors.create(payload);
    } else {
      connector = connectors[0];
      connector = await api.connectors.fetch(connectors[0].connectorId);
      if (!connector.version || (connector.version !== this.version)) {
        const updated = {
          connectorId: connector.connectorId,
          version: this.configuration.version,
          description: this.configuration.description,
          readme: this.configuration.readme,
          orgWideAccess: this.configuration.orgWideAccess,
          logoUrl: this.configuration.logoUrl,
          connection: this.configuration.connectionSettings.clone(),
          settings: this.configuration.settings.clone(),
          webhook: this.configuration.webhook.url && topics.length ? { ...this.configuration.webhook, ...{ apikey: this.configuration.webhook.apiKey || "" }, topics } : null,
          entities: mappings
        }
        await api.connectors.update(updated);
      } else {
        logger.info('connector:register:update:skip', { id: connector.connectorId });
      }
    }
    logger.info('connector:register:done', { id: connector.connectorId });
    return connector;
  }
  async unregister() {
    const name = this.configuration.name;
    logger.info('connector:unregister', { name });

    const api = new API({ api: this.configuration.api_configuration });

    const connectors = await api.connectors.list(name);
    if (connectors.length === 1) {
      const connector = await api.connectors.fetch(connectors[0].connectorId);
      if (connector.status !== api.ConnectorStatus.Deleted) {
        await api.connectors.remove(connector.connectorId);
      } else {
        logger.warn('connector:unregister:already_removed', { name });
        return false;
      }
    } else {
      logger.warn('connector:unregister:not_found', { name });
      return false;
    }
    logger.info('connector:unregister:done', { name });
    return true;
  }

  async registerIntegration(name, apiKey) {
    const connectorName = this.configuration.name;
    const integrationName = name || connectorName;
    logger.info('integration:register', { connectorName, integrationName });

    const api = new API({ api: { ...this.configuration.api_configuration, ...{ apiKey: apiKey } } });
    const result = await api.integrations.create({
      connectorName: connectorName,
      environment: 'test',
      integrationName: integrationName,
      connection: this.configuration.connectionSettings.clone(),
      settings: this.configuration.settings.clone(),
      entities: this.configuration.mappings.mappings,
      status: IntegrationStatus.Configured,
    });
    const integration = await api.integrations.fetch(result.integrationId);
    logger.info('integration:register:done', { integration, connectorName, integrationName });
    return integration;
  }
  async updateIntegration(name, apiKey) {
    const connectorName = this.configuration.name;
    const integrationName = name || connectorName;
    logger.info('integration:update', { connectorName, integrationName });

    const api = new API({ api: { ...this.configuration.api_configuration, ...{ apiKey: apiKey } } });
    const integrations = (await api.integrations.list({ connector: connectorName })).filter(x => x.integrationName === integrationName);
    if (integrations.length !== 1) {
      throw new Error('not_found');
    }
    const result = await api.integrations.update({
      integrationId: integrations[0].integrationId,
      integrationName: integrationName,
      connection: this.configuration.connectionSettings.clone(),
      settings: this.configuration.settings.clone(),
      entities: this.configuration.mappings.mappings,
      status: IntegrationStatus.Configured,
    });
    logger.info('integration:update', { result, connectorName, integrationName });
    return result;
  }
  async unregisterIntegration(name, apiKey) {
    const connectorName = this.configuration.name;
    const integrationName = name || connectorName;
    logger.info('integration:remove', { connectorName, integrationName });

    const api = new API({ api: { ...this.configuration.api_configuration, ...{ apiKey: apiKey } } });
    const integrations = (await api.integrations.list({ connector: connectorName })).filter(x => x.integrationName === integrationName);
    if (integrations.length !== 1) {
      throw new Error('not_found');
    }
    const result = await api.integrations.remove(integrations[0].integrationId);
    logger.info('integration:remove', { result, connectorName, integrationName });
    return result;
  }

  async upgradeIntegrations(status) {
    if (status && !Array.isArray(status)) {
      status = [status];
    }
    logger.info('integrations:upgrade', { status });

    const api = new API({ api: this.configuration.api_configuration });
    const integrations = await api.integrations.list({ status, keys: 'integrationId,status,connectorId' }).catch((error) => {
      logger.info('integrations:upgrade:list:failed', { status, error });
      return Promise.reject(error)
    });
    for (let integration of integrations) {
      const integrationId = integration.integrationId;
      logger.info('integrations:upgrade:item', { integrationId, integration });

      const result = await api.integrations.upgrade(integrationId).catch((error) => {
        logger.info('integrations:upgrade:failed', { integrationId, error });
        // ignore error to continue
      });
      result;

      logger.info('integrations:upgrade:item:done', { integrationId });
    }

    logger.info('integrations:upgrade:done', { status });
  }
  async upgradeIntegration(integrationId) {
    logger.info('integration:upgrade', { integrationId });

    const api = new API({ api: this.configuration.api_configuration });
    const result = await api.integrations.upgrade(integrationId).catch((error) => {
      logger.info('integration:upgrade:failed', { integrationId, error });
      return Promise.reject(error)
    });

    logger.info('integration:upgrade', { result, integrationId });
    return result;
  }

}

module.exports = {
  ConnectorMeta
}