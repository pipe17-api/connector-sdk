Typical connector project consist of following components
- connector SDK - implements basic flows and provides access to PIPE17 API. This is common for all connectors
- vendor API - implements wrapper on top of vendor API that allows easy consumption of vendor data in connector. This is implemented per vendor and should be PIPE17 agnostic.
usually we take swagger spec and generate client library which is wrapped to provide generic error handling and simplify data structures if necessary.
Important vendor API should be testable without connector SDK or any of PIPE17 infrastructure. SDK will work with vendor layer that implements one of expected/supported strategies 
for pulling/pushing data (discussed below)
- glue component - glues connector SDK with vendorAPI
- mappings definitions - define data transformation between vendor API component and PIPE17 API

For simplicity we should consider vendor API as primary task for bringing new vendor, since covering vendor API component also covers above 75% of connector codebase.

Entities supported by connectors
- products
- orders
- shipments
- fulfillments
- locations
- inventory
- arrivals
- receipts

Connector types:
- 3pl. usual suspects
  - products to vendor (out)
  - shipments to vendor (out)
  - fulfillments from vendor (in)
  - locations from vendor (in)
  - inventory from vendor (in)
  - arrivals to vendor (out)
  - receipts from vendor (in)
- ecomm. usual suspects
  - orders from vendor (in)
  - fulfillments to vendor (out)
  - inventory to vendor (out)
  - products (sometimes) from vendor (in)

While integrating with vendor following are generic strategies to receive data from vendor
- incremental changes sync (always preferred)
  - this assumes vendor provides a feed of entities that is filterable and orderable in the same time by certain timestamp
  - this is always preferred way of pulling changes, since you can process as mny things as you want at once and then continue from where you stopped
  - expectation is that vendor API will implement lazy loading list with configurable filter and paging criteria and will wrap result into JS iterator, so that connector SDK will do it.next() to get next item and process it.
  - in some cases only filterable feed is available from vendor, but it is not orderable via API means. Important considerations for such cases
    - vendor API component will have to implement sorting in code
    - proper sorting only will be possible when you can get continuos yet limited data chunk from API (i.e. fetching million records just to apply sort is not an option)
    - this usually means that API should support filtering that limits date range on top margin along with bottom margin. if so you can always (almost) find upper bound for given timestamp that will produce reasonable set of data (e.g. 100 records), sort obtained records and feed via iterator to connector SDK. next time new timestamp will be used so this window will always be moving till NOW and eventually will be exhausted.
- webhook (if supported by vendor)
  - in case if vendor provides webhook registration, it is beneficial to have webhooks support to get faster updates from vendor, however for reliability if vendor has incremental changes feed, it should also be processing along with webhooks handler. Some de-duplication logic might be required in glue component.
  - in some cases vendor will provide webhook but not incremental changes feed, in such case it is ok to rely on webhook only
  - usually webhook payloads include item ids, vendor API component should have methods allowing to retrieve items referred in webhook payload and provide results to glue component, which is very similar to list, with exception that for webhook vendor API will provide just a single item
  - in case if vendor groups multiple items in webhook payload, vendor API should support that and feed connector SDK with a list of affected items
- comparing exhaustible set of objects
  - In some cases vendor will not have neither incremental changes feed not webhook
  - For such cases it will be important to identify criteria _exhaustible_ set of objects on PIPE17 and on vendor side, size of this set should be as small as possible
  - if size can not be guarantied to be small, then connector will have to be adjusted to do heavy lifting at dedicated time intervals (e.g. off working hours)
  - Example. 3PL provider, can accept arrivals and will update them eventually with received counts. Problem - no webhooks, no incremental changes feed. approach:
    - take them most old arrival on PIPE17 side that is not fully received yet, picks its timestamp
    - take all records denoting receipts on vendor side (apply filter if exist/necessary)
    - do hashmap comparison of 2 sets, apply changes to PIPE17 side
    - idea is that most old arrival will eventually be closed and thus set size will be reduced
    - to be safe in case if this does not happen, have upper bound for arrival lifetime, e.g. 6 months
- comparing set of objects (should be avoided)
  - in case if vendor does not provide any relevant filters, there is no other choice except fetching whole dataset from vendor and compare it to data on PIPE17 side.
  - this process should run only at allowed intervals
- batch import
  - sometimes you need to get a fresh copy of data from vendor for all tracked entities
  - example: nightly inventory re-conciliatiopn
  - important. data sets here might be huge, so vendor API should lazy-load data pages and feed to connector SDK as requested

For pushing data from pipe17 to vendor API, usually it is way simpler, since for each entity type PIPE17 supports
- incremental changes feed with proper filters (can be extended if required), paging and sorting
- webhooks can also be registered to get data changes applied faster

Data linkage and de-duplication 
- PIPE17 is capable of storing external id of item for all entities - this can be used for further lookup and dedup tasks while communicating with vendor.

Workflow for each vendor
- get test account
- review API
- generate client library from API spec or provide own implementation
- identify per-customer connectivity parameters (e.g. api url, apiKey, other configuration values which are required and can not be determined on runtime)
- vendor API component
  - API initialization with loaded configuration
  - First time init (if required, e.g. register webhooks etc)
  - methods to fetch or apply data
  - validate conformance to one of sync strategies above
  - have tests executable standalone for all implemented methods
- get connector key
- declare connector manifest - entities, settings etc
- register PIPE17 test org
- register connector
- register integration
- implement data transformation mappings
- implement glue component
