const { API } = require('../api');
const fs = require('fs');
const path = require('path');

async function test() {
  const api = new API({
    api: {
      basePath: 'https://api-v3.develop.pipe17.com/api/v3',
      apiKey: '66e3eec311834222a782ad3e7a27b0e259f46868408bbbf74b18c799e823b0af'
    }
  });

  const created = await api.labels.upsert({
    contentType: 'image/png',
    fileName: 'labels.png'
  });
  const labelId = created.labelId
  const fetched = await api.labels.fetch(labelId);
  const buffer = fs.readFileSync(path.join(__dirname, 'labels.png'));
  const srcBytes = buffer.buffer.slice(buffer.byteOffset, buffer.byteOffset + buffer.byteLength);
  const uploaded = await api.labels.upload(fetched.labelId, srcBytes);
  const downloaded = await api.labels.download(labelId);

  const dstBytes = Buffer.from(Buffer.from(downloaded).toString('ascii'), 'base64');
  const srcView = new Uint8Array(srcBytes);
  const dstView = new Uint8Array(dstBytes);
  if (srcView.length !== dstView.length) {
    throw new Error('unmatched_length');
  }
  const unmatched = srcView.map((v, i) => (dstView[i] !== v ? [i, v, dstView[i]] : undefined)).filter((v) => !!v);
  if (unmatched.length !== 0) {
    throw new Error('unmatched_content');
  }
}

test().then(() => {
  debugger;
}).catch((error) => {
  debugger;
})