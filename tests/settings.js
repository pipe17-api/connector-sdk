const { ConnectorMeta, ConnectorType, ValueType, EntityType, TriggerTypes, Direction } = require('../');
const { API } = require('../api');
const { ConnectorContext } = require('../context');

const shipmentMappingToP17      = 'd0547259-28ac-4a24-b350-cf4c57fb1b00';
const fulfillmentMappingFromP17 = 'd0547259-28ac-4a24-b350-cf4c57fb1b01';

const definition = new ConnectorMeta()
.withName(process.env.P17SDK_SERVICE_ID + ':test1')
.withType(ConnectorType.Ecommerce)
.withLogoUrl(process.env.P17SDK_CONNECTOR_LOGO_URL)
.withAPIConfiguration({ 
  basePath: process.env.P17SDK_CONNECTOR_API_PATH, 
  apiKey: process.env.P17SDK_CONNECTOR_API_KEY 
})
.withSchemaAPIConfiguration({ 
  basePath: process.env.P17SDK_SCHEMA_API_PATH, 
  apiKey: process.env.P17SDK_SCHEMA_API_KEY 
})
.withWebHook(process.env.P17SDK_CONNECTOR_WEBHOOK_URL, process.env.P17SDK_CONNECTOR_WEBHOOK_KEY)
.withConnectionSetting({ path: 'APIUrl', type: ValueType.String, secret: false, options: { defaultValue: process.env.P17SDK_MOCK_ECOMM_URL, label: "API Url" } })
.withConnectionSetting({ path: 'APIKey', type: ValueType.String, secret: true, options: { defaultValue: process.env.P17SDK_MOCK_ECOMM_APIKEY, label: "API Key" } })
.withSetting({ path: 'Test1', type: ValueType.String, options: { defaultValue: '1234', label: "Test" } })
.withSetting({ path: 'shipments-in-updated', type: ValueType.String })
.withSetting({ path: 'fulfillments-out-updated', type: ValueType.String })
.withMapping({ entityType: EntityType.Shipments, direction: Direction.In, mappingId: shipmentMappingToP17, })
.withMapping({ entityType: EntityType.Fulfillments, direction: Direction.Out, mappingId: fulfillmentMappingFromP17 });

definition.entities.fulfillments.withTrigger(TriggerTypes.P17Event);

async function test() {
  //const registered = await definition.register(true, './tests/mappings');
  //const integration = await definition.registerIntegration(definition.configuration.name + '-integration', definition.configuration.api_configuration.apiKey);

  const connector = definition.build();
  const context = new ConnectorContext(connector);
  const integrationId = '993b1a9ba18b5c8c';
  const prepared = await context.prepare({ integrationId });
  const timestamp = await connector.lastTimestamp(prepared, '', 'shipments', 'in');
  timestamp;
  await connector.setTimestamp(prepared, 'shipments', 'in', 0, true, new Date().toISOString())
  const timestamp1 = await connector.lastTimestamp(prepared, '', 'shipments', 'in');
}
test().then((x) => {
  debugger;
});