const { ConnectorType, ConnectorSettingsFieldTypeEnum, EntityName, Direction } = require('@pipe17/api-client');
const { API } = require('./api')

const EntityType = EntityName;
const ValueType = ConnectorSettingsFieldTypeEnum

module.exports = {
  API,
  ConnectorType,
  ValueType,
  EntityType,
  Direction
}