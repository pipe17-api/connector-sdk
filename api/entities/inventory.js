const { APIAccessor, logger, chunk } = require('../accessor');
APIAccessor;

class Inventory {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('inventory:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const inventory = await this.list(opt);
      
      let updatedAt = null;
      const filtered = inventory.filter((x) => {
        const id = [x.inventoryId, x.locationId, x.sku].join('$');
        if (hash.previous[id]) {
          return false;
        }
        if (!updatedAt || (updatedAt < x.updatedAt)) {
          updatedAt = x.updatedAt;
        }
        hash.current[id] = 1;
        return true;
      });
    
      if (opt.batch) {
        if (filtered.length) {
          const report = {
            items: filtered,
            updatedAt: updatedAt
          }
          const r = await (yield report);
          r;
        }
      } else {
        for (let inventoryItem of filtered) {
          const r = await (yield inventoryItem);
          r;
        }
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, inventory)
      } else if (opt._nextAsync) {
        await opt._nextAsync(opt, inventory)
      } else if (opt.order === 'updatedAt') {
        const last = inventory[inventory.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (inventory.length < opt.count)
    }

    logger.info('inventory:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('inventory:list', { opt });
    const result = await this.raw_api.listInventory({ ...opt }).catch((error) => {
      if ((error.isP17Failure) && (error.failure.code === 404)) {
        return { inventory: [] };
      }
      throw error;
    });
    const inventory = result.inventory;
    logger.info('inventory:list:done', { inventory });
    return inventory;
  }
  /**
   * @param {string|{ inventoryId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { inventoryId: id } : id ;
    logger.info('inventory:fetch', { query });
    const result = await this.raw_api.fetchInventory(query);
    const inventory = result.inventory;
    logger.info('inventory:fetch:done', { inventory });
    return inventory;
  }

  /**
   * @param {import('@pipe17/api-client').Inventory} item
   */
  async upsert(item) {
    if (item.inventory){
      logger.info('inventory:upsert', { count: item.inventory.length });
      const chunked = chunk(item.inventory);
      const promises = chunked.map((c, index) => {
        logger.info('inventory:upsert:chunk', { index, count: c.length });
        // array of 10 ingest inventory items
        return this.raw_api.createInventory({
          inventoryCreateData: c
        }).catch((error) => {
          logger.error("inventory:upsert:chunk", { index, count: c.length, error })
          return Promise.reject(error);
        });        
      });
      return await Promise.all(promises);
    } else if (["ingest", "adjust"].includes(item.event)){
      logger.info('inventory:upsert', { item });

      const result = await this.raw_api.createInventory({
        inventoryCreateData: [item]
      });        
    } else{
      logger.info('inventory:upsert', { item });
      const result = await this.raw_api.updateInventory({
        inventoryUpdateRequest: item
      });        
    }      
    logger.info('inventory:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Inventory
}