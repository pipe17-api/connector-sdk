const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Receipts {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('receipts:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const receipts = await this.list(opt);
      for (let receipt of receipts) {
        const id = receipt.receiptId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield receipt);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, receipts)
      } else if (opt.order === 'updatedAt') {
        const last = receipts[receipts.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (receipts.length < opt.count)
    }

    logger.info('receipts:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('receipts:list', { opt });
    const result = await this.raw_api.listReceipts({ ...opt }).catch((error) => {
      if ((error.isP17Failure) && (error.failure.code === 404)) {
        return { receipts: [] };
      }
      throw error;
    });
    const receipts = result.receipts;
    logger.info('receipts:list:done', { receipts });
    return receipts;
  }
  /**
   * @param {string|{ receiptId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { receiptId: id } : id ;
    logger.info('receipts:fetch', { query });
    const result = await this.raw_api.fetchReceipt(query);
    const receipt = result.receipt;
    logger.info('receipts:fetch:done', { receipt });
    return receipt;
  }
  /**
   * @param {import('@pipe17/api-client').Receipt} item
   */
  async upsert(item) {
    logger.info('receipts:upsert', { item });
    
    let createReceipt = false;
    if (!item.skipAdjust) {
      const result = await this.raw_api.fetchArrival({
        arrivalId: item.arrivalId
      }).catch((error) => {
        logger.error('receipts:upsert:lookup:failed', { error, item })
        return Promise.reject(error);
      });
      const arrival = result.arrival;

      item.lineItems.forEach((received) => {
        const expected = arrival.lineItems.find((x) => x.sku === received.sku) || {};
        const quantity = received.quantity - (expected.receivedQuantity || 0);
        if (quantity !== 0) {
          createReceipt = true;
          received.quantity = quantity;
        }
      });
    } else {
      createReceipt = true;
    }

    if (createReceipt) {
      const receipt = await this.raw_api.createReceipts({
        receiptCreateData: [item]
      }).catch((error) => {
        logger.error('receipts:upsert:failed', { error, item })
        return Promise.reject(error);
      });
      item.receiptId = receipt.receiptId;
    } else {
      logger.info('receipts:upsert:skip', { item })
    }
    
    logger.info('receipts:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Receipts
}