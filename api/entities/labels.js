const Blob = require('fetch-blob');
const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Labels {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('labels:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const labels = await this.list(opt);
      for (let label of labels) {
        const id = label.labelId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield label);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, labels);
      } else if (opt.order === 'updatedAt') {
        const last = labels[labels.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (labels.length < opt.count);
    }

    logger.info('labels:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('labels:list', { opt });
    const result = await this.raw_api.listLabels({ ...opt }).catch((error) => {
      if ((error.isP17Failure) && (error.failure.code === 404)) {
        return { labels: [] };
      }
      return Promise.reject(error);
    });
    const labels = result.labels;
    logger.info('labels:list:done', { labels });
    return labels;
  }
  /**
   * @param {string|{ labelId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { labelId: id } : id ;
    logger.info('labels:fetch', { query });
    const result = await this.raw_api.fetchLabel(query);
    const label = result.label;
    logger.info('labels:fetch:done', { label });
    return label;
  }
  /**
   * @param {import('@pipe17/api-client').Label} item
   */
  async upsert(item) {
    logger.info('labels:upsert', { item });

    if (!item.labelId) {
      const result = await this.raw_api.createLabel({
        labelCreateData: item
      }).catch((error) => {
        logger.error('labels:upsert:failed', { error, item });
        return Promise.reject(error);
      });
      item.labelId = result.label.labelId;
    }

    logger.info('labels:upsert:done', { item });
    return item;
  }

  async upload(id, buffer) {
    logger.info('labels:upload', { id });

    const body = new Blob([buffer])

    const result = await this.raw_api.uploadLabelContent({
      labelId: id,
      body: body
    }).catch((error) => {
      logger.error('labels:upload:failed', { error, id });
      return Promise.reject(error);
    });

    logger.info('labels:upload:done', { id });
    return result;
  }

  async download(id) {
    logger.info('labels:download', { id });

    const result = await this.raw_api.downloadLabelContent({
      labelId: id
    }).catch((error) => {
      logger.error('labels:download:failed', { error, id });
      return Promise.reject(error);
    });

    logger.info('labels:download:done', { id });
    return await result.arrayBuffer();
  }
}

module.exports = {
  Labels
}