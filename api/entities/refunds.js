const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Refunds {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('refunds:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const refunds = await this.list(opt);
      for (let refund of refunds) {
        const id = refund.refundId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield refund);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, refunds)
      } else if (opt.order === 'updatedAt') {
        const last = refunds[refunds.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (refunds.length < opt.count)
    }

    logger.info('refunds:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('refunds:list', { opt });
    const result = await this.raw_api.listRefunds({ ...opt }).catch((error) => {
      logger.error('refunds:list:failed', { opt, error });
      return Promise.reject(error);
    });
    const refunds = result.refunds;
    logger.info('refunds:list:done', { opt, count: refunds.length });
    return refunds;
  }
  /**
   * @param {string|{ refundId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { refundId: id } : id ;
    logger.info('refunds:fetch', { query });
    const result = await this.raw_api.fetchRefund(query).catch((error) => {
      if (error && error.failure && error.failure.code === 404) {
        return { refund: null };
      }
      logger.error('refunds:fetch:failed', { query, error });
      return Promise.reject(error);
    });
    const refund = result.refund;
    logger.info('refunds:fetch:done', { query, refund });
    return refund;
  }
  /**
   * @param {import('@pipe17/api-client').Refund} item
   */
  async upsert(item) {
    logger.info('refunds:upsert', { item });
    if (!item.refundId) {
      const result = await this.raw_api.createRefund({
        refundCreateRequest: item
      }).catch((error) => {
        logger.error('refunds:create:failed', { item, error });
        return Promise.reject(error)
      });
      item.refundId = result.refunds[0].refund.refundId;
    } else {
      const result = await this.raw_api.updateRefund({
        refundId: item.refundId,
        refundUpdateRequest: item
      }).catch((error) => {
        logger.error('refunds:update:failed', { item, error });
        return Promise.reject(error)
      });
      result;
    }

    logger.info('refunds:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Refunds
}