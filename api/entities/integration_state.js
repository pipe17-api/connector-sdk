const { APIAccessor, logger } = require('../accessor');
APIAccessor;
const api = require('@pipe17/api-client');
api;

class IntegrationState {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.pvt_api;
  }

  /**
   * @param {string} id 
   * @param {string} [defaultValue] 
   */
  async fetch(id, defaultValue) {
    logger.info('integration_state:fetch', { id });
    const result = await this.raw_api.fetchIntegrationState({ 
      name: id 
    }).catch((error) => {
      //logger.warn('integration_state:fetch:failed', { id, error });
      return { integrationState: { value: defaultValue || null } };
    });
    const success = result.success
    return result.integrationState.value;
  }

  /**
   * @param {string} id 
   * @param {string} value 
   */
  async update(id, value) {
    logger.info('integration_state:update', { id, value });
    const result = await this.raw_api.upsertIntegrationState({ 
      integrationStateCreateData: { name: id, value: value } 
    }).catch((error) => {
      logger.error('integration_state:update:failed', { id, error });
      return Promise.reject(error);
    });
    const success = result.success
    logger.info('integration_state:update:done', { success, id });
    return success;
  }

  /**
   * @param {string} id 
   */
  async remove(id) {
    logger.info('integration_state:remove', { id });
    const result = await this.raw_api.upsertIntegrationState({ 
      integrationStateCreateData: { name: id } 
    }).catch((error) => {
      logger.error('integration_state:remove:failed', { id, error });
      return Promise.reject(error);
    });
    const success = result.success
    logger.info('integration_state:remove:done', { success, id });
    return success;
  }
}

module.exports = {
  IntegrationState
}