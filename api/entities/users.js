const { APIAccessor } = require('../accessor');
APIAccessor;

class Users {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }
}

module.exports = {
  Users
}