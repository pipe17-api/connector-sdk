const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Locations {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('locations:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const locations = await this.list(opt)
      for (let location of locations) {
        const id = location.locationId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield location);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, locations)
      } else if (opt.order === 'updatedAt') {
        const last = locations[locations.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (locations.length < opt.count)
    }

    logger.info('locations:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('locations:list', { opt });
    const result = await this.raw_api.listLocations({ ...opt }).catch((error) => {
      logger.error('locations:list:failed', { opt, error });
      return Promise.reject(error);
    });
    const locations = result.locations;
    logger.info('locations:list:done', { locations });
    return locations;
  }
  /**
   * @param {string|{ locationId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { locationId: id } : id ;
    logger.info('locations:fetch', { query });
    const result = await this.raw_api.fetchLocation(query).catch((error) => {
      logger.error('locations:fetch:failed', { query, error });
      return Promise.reject(error);
    });
    const location = result.location;
    logger.info('locations:fetch:done', { location });
    return location;
  }
  /**
   * @param {import('@pipe17/api-client').Location} item
   */
  async upsert(item) {
    logger.info('locations:upsert', { item });

    if (item.externalSystem && item.externalSystem.length) {
      const query = { 
        keys: '*',
        extLocationId: item.externalSystem[0].locationValue,
        extIntegrationId: item.externalSystem[0].integrationId,
      };
      const existing = await this.list(query).catch((error) => {
        logger.error('locations:lookup:failed', { query, error });
        return Promise.reject(error);
      });
      if (existing.length) {
        item.locationId = existing[0].locationId;
        // we do not need to update extLocationId, since it is already stored in db
        item.externalSystem = undefined;
      }
    }
    
    if (!item.locationId) {
      const result = await this.raw_api.createLocation({
        locationCreateRequest: item
      }).catch((error) => {
        logger.error('locations:create:failed', { item, error });
        return Promise.reject(error);
      });
      item.locationId = result.location.locationId;
    } else {
      const result = await this.raw_api.updateLocation({
        locationId: item.locationId,
        locationUpdateRequest: item,
      }).catch((error) => {
        logger.error('locations:update:failed', { item, error });
        return Promise.reject(error);
      });
      result;
    }
    
    logger.info('locations:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Locations
}