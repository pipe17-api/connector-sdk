const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Arrivals {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('arrivals:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const arrivals = await this.list(opt);
      for (let arrival of arrivals) {
        const id = arrival.arrivalId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield arrival);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, arrivals)
      } else if (opt.order === 'updatedAt') {
        const last = arrivals[arrivals.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (arrivals.length < opt.count)
    }

    logger.info('arrivals:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('arrivals:list', { opt });
    const result = await this.raw_api.listArrivals({ ...opt }).catch((error) => {
      if ((error.isP17Failure) && (error.failure.code === 404)) {
        return { arrivals: [] };
      }
      throw error;
    });
    const arrivals = result.arrivals;
    logger.info('arrivals:list:done', { arrivals });
    return arrivals;
  }
  /**
   * @param {string|{ arrivalId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { arrivalId: id } : id ;
    logger.info('arrivals:fetch', { query });
    const result = await this.raw_api.fetchArrival(query);
    const arrival = result.arrival;
    logger.info('arrivals:fetch:done', { arrival });
    return arrival;
  }
  /**
   * @param {import('@pipe17/api-client').Arrival} item
   */
  async upsert(item) {
    logger.info('arrivals:upsert', { item });

    if (!item.arrivalId) {
      if (item.extArrivalId) {
        const existing = this.fetch("ext:" + item.extArrivalId).catch(() => null);
        if (existing) {
          item.arrivalId = existing.arrivalId;
        }
      }
    }

    if (!item.arrivalId) {
      const result = await this.raw_api.createArrivals({
        arrivalCreateData: [item]
      });
      item.arrivalId = result.arrivals[0].arrival.arrivalId;
    } else {
      const result = await this.raw_api.updateArrival({
        arrivalId: item.arrivalId,
        arrivalUpdateRequest: item,
      });
      result;
    }

    logger.info('arrivals:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Arrivals
}