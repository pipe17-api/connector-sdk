const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Events {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('events:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const events = await this.list(opt)
      for (let event of events) {
        const id = event.eventId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield event);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, events);
      } else if (opt.order === 'updatedAt') {
        const last = events[events.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (events.length < opt.count);
    }

    logger.info('events:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('events:list', { opt });
    const result = await this.raw_api.listEvents({ ...opt }).catch((error) => {
      logger.error('events:list:failed', { opt, error });
      return Promise.reject(error)
    });
    const events = result.events;
    logger.info('events:list:done', { events });
    return events;
  }
  /**
   * @param {string|{ eventId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { eventId: id } : id ;
    logger.info('events:fetch', { query });
    const result = await this.raw_api.fetchEvent(query).catch((error) => {
      logger.error('events:fetch:failed', { query, error });
      return Promise.reject(error)
    });
    const event = result.event;
    logger.info('events:fetch:done', { event });
    return event;
  }
  /**
   * @param {import('@pipe17/api-client').Event} item
   */
  async upsert(item) {
    logger.info('events:upsert', { item });

    if (!item.eventId) {
      const result = await this.raw_api.createEvent({
        eventCreateData: item,
      }).catch((error) => {
        logger.error('events:create:failed', { error });
        return Promise.reject(error)
      });
      item.eventId = result.event.eventId;
    } else {
      const result = await this.raw_api.updateEvent({
        eventId: item.eventId,
        eventUpdateRequest: item
      }).catch((error) => {
        logger.error('events:update:failed', { error });
        return Promise.reject(error)
      });
      result;
    }
   
    logger.info('events:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Events
}