const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Webhooks {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

   
  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('webhooks:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const webhooks = await this.list(opt)
      for (let webhook of webhooks) {
        const id = webhook.webhookId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield webhook);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, webhooks);
      } else if (opt.order === 'updatedAt') {
        const last = webhooks[webhooks.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (webhooks.length < opt.count);
    }

    logger.info('webhooks:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('webhooks:list', { opt });
    const result = await this.raw_api.listWebhooks({ ...opt }).catch((error) => {
      if ((error.isP17Failure) && (error.failure.code === 404)) {
        return { webhooks: [] };
      }
      throw error;
    });
    const webhooks = result.webhooks;
    logger.info('webhooks:list:done', { webhooks });
    return webhooks;
  }
  /**
   * @param {string|{ webhookId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { webhookId: id } : id ;
    logger.info('webhooks:fetch', { query });
    const result = await this.raw_api.fetchWebhook(query);
    const webhook = result.webhook;
    logger.info('webhooks:fetch:done', { webhook });
    return webhook;
  }
  /**
   * @param {import('@pipe17/api-client').Webhook} item
   */
  async upsert(item) {
    logger.info('webhooks:upsert', { item });

    // const result = await this.raw_api.updateIntegration({
    //   integrationId: context.integration.integrationId,
    //   settings: context.configuration.settings.clone(),
    // });
   
    logger.info('webhooks:upsert:done', { item });
    return item;
  }




}

module.exports = {
  Webhooks
}