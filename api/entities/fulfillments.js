const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Fulfillments {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('fulfillments:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const fulfillments = await this.list(opt)
      for (let fulfillment of fulfillments) {
        const id = fulfillment.fulfillmentId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield fulfillment);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, fulfillments)
      } else if (opt.order === 'updatedAt') {
        const last = fulfillments[fulfillments.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (fulfillments.length < opt.count)
    }

    logger.info('fulfillments:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    opt.keys = '*';
    opt.order = 'updatedAt';

    logger.info('fulfillments:list', { opt });
    const result = await this.raw_api.listFulfillments({ ...opt }).catch((error) => {
      logger.error('fulfillments:list:failed', { opt, error });
      return Promise.reject(error);
    });
    const fulfillments = result.fulfillments;
    logger.info('fulfillments:list:done', { fulfillments });
    return fulfillments;
  }
  /**
   * @param {string|{ fulfillmentId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { fulfillmentId: id } : id ;
    logger.info('fulfillments:fetch', { query });
    const result = await this.raw_api.fetchFulfillment(query).catch((error) => {
      logger.error('fulfillments:fetch:failed', { query, error });
      return Promise.reject(error);
    });
    const fulfillment = result.fulfillment;
    logger.info('fulfillments:fetch:done', { fulfillment });
    return fulfillment;
  }
  /**
   * @param {import('@pipe17/api-client').Fulfillment} item
   */
  async upsert(item) {
    logger.info('fulfillments:upsert', { item });

    const result = await this.raw_api.createFulfillments({
      fulfillmentCreateData: [item]
    }).catch((error) => {
      logger.error('fulfillments:create:failed', { item, error });
      return Promise.reject(error);
    });
    item.fulfillmentId = result.fulfillments[0].fulfillment.fulfillmentId;
    logger.info('fulfillments:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Fulfillments
}