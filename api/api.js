const { APIAccessor } = require('./accessor');

const { Schema } = require('./entities/schema');

const { Accounts } = require('./entities/accounts');
const { APIKeys } = require('./entities/apikeys');
const { Arrivals } = require('./entities/arrivals');
const { Connectors } = require('./entities/connectors');
const { Events } = require('./entities/events');
const { Exceptions } = require('./entities/exceptions');
const { Fulfillments } = require('./entities/fulfillments');
const { Integrations } = require('./entities/integrations');
const { IntegrationState } = require('./entities/integration_state');
const { Labels } = require('./entities/labels');
const { Locations } = require('./entities/locations');
const { Inventory } = require('./entities/inventory');
const { Mappings } = require('./entities/mappings');
const { Orders } = require('./entities/orders');
const { Organizations } = require('./entities/organizations');
const { Products } = require('./entities/products');
const { Receipts } = require('./entities/receipts');
const { Refunds } = require('./entities/refunds');
const { Returns } = require('./entities/returns');
const { Routings } = require('./entities/routings');
const { Shipments } = require('./entities/shipments');
const { Users } = require('./entities/users');
const { Webhooks } = require('./entities/webhooks');

class API extends APIAccessor {
  /**
   * @param {Object} configuration
   * @param {import('../configuration/api').APIConfiguration} configuration.api
   * @param {import('../configuration/api').APIConfiguration} configuration.schema_api
   */
  constructor(configuration) {
    super(configuration)
   
    this.accounts = new Accounts(this);
    this.apikeys = new APIKeys(this);
    this.arrivals = new Arrivals(this);
    this.connectors = new Connectors(this);
    this.events = new Events(this);
    this.exceptions = new Exceptions(this);
    this.fulfillments = new Fulfillments(this);
    this.integrations = new Integrations(this);
    this.integration_state = new IntegrationState(this);
    this.labels = new Labels(this);
    this.locations = new Locations(this);
    this.inventory = new Inventory(this);
    this.mappings = new Mappings(this);
    this.orders = new Orders(this);
    this.organizations = new Organizations(this);
    this.products = new Products(this);
    this.routings = new Routings(this);
    this.receipts = new Receipts(this);
    this.refunds = new Refunds(this);
    this.returns = new Returns(this);
    this.shipments = new Shipments(this);
    this.users = new Users(this);
    this.webhooks = new Webhooks(this);
    this.schema = new Schema(this);
  }
}

module.exports = {
  API
}