class APIConfiguration {
  /**
   * @param {Object} configuration 
   * @param {string} configuration.basePath
   * @param {string} configuration.apiKey
   * @param {boolean} configuration.forceKey
   */
  constructor(configuration) {
    this.basePath = configuration.basePath;
    this.apiKey = configuration.apiKey;
    this.forceKey = configuration.forceKey;
  }
}

module.exports = {
  APIConfiguration
}