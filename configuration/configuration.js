const { APIConfiguration } = require("./api");
const { Mappings } = require("./mappings");
const { Settings, ConnectionSettings } = require("./settings");
const { WebhookConfiguration } = require("./webhook");
const { EntityHandlers } = require("../meta/entities");

class Configuration {
  constructor() {
    this.name = 'p17sdk-connector';
    this.description = '';
    this.type = '';
    this.logoUrl = '';
    this.version = '1.0.0'
    this.orgWideAccess = false;
    this.api_configuration = new APIConfiguration({});
    this.schema_api_configuration = new APIConfiguration({});
    this.entities = new EntityHandlers();
    this.mappings = new Mappings();
    this.connectionSettings = new ConnectionSettings();
    this.settings = new Settings();
    this.webhook = new WebhookConfiguration({});
  }
  clone() {
    const cloned = new Configuration();
    cloned.name = this.name;
    cloned.description = this.description;
    cloned.type = this.type;
    cloned.logoUrl = this.logoUrl;
    cloned.version = this.version;
    cloned.orgWideAccess = this.orgWideAccess;

    cloned.api_configuration = new APIConfiguration({...this.api_configuration});
    cloned.schema_api_configuration = new APIConfiguration({...this.schema_api_configuration});

    cloned.entities = new EntityHandlers();
    cloned.entities._entities = { ...this.entities._entities };

    cloned.mappings = new Mappings({...this.mappings.mappings});
    cloned.connectionSettings = this.connectionSettings.clone();
    cloned.settings = this.settings.clone();
    cloned.webhook = new WebhookConfiguration({...this.webhook});

    return cloned;
  }
}

module.exports = {
  Configuration
}