class WebhookConfiguration {
  /**
   * @param {Object} configuration 
   * @param {string} configuration.url
   * @param {string} configuration.apiKey
   */
  constructor(configuration) {
    this.url = configuration.basePath;
    this.apiKey = configuration.apiKey;
  }
}

module.exports = {
  WebhookConfiguration
}