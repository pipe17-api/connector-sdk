
const { test, } = require('@jest/globals');
const { format } = require('logform')
const { defaultLogger, redactSecrets, createLogger } = require('./logger');

const fmt = format.combine(
    format.timestamp(),
    format((info) => redactSecrets(info))(),
    format.json());


const lg = createLogger({ format: fmt }).child({ module: 'mock-3pl' })
const logger = defaultLogger.child({ module: 'mock-3pl' });

test('', async () => {

    lg.info('WINSxxx')
    logger.info('WINSTON');

    lg.info(12345)
    logger.info(54321);

    lg.info({ fst: 'WINSxxx', password: "xxxxx" })
    logger.info({ fst: 'WINSTON', password: "xxxxx" });

    lg.info('WINSxxx', { snd: 'WINSxxx' })
    logger.info('WINSTON', { snd: 'WINSTON' });

    lg.info(12345, { snd: 'WINSxxx' })
    logger.info(54321, { snd: 'WINSTON' });

    lg.info({ fst: 'WINSxxx' }, { snd: 'WINSxxx' })
    logger.info({ fst: 'WINSTON' }, { snd: 'WINSTON' });

    lg.info('WINSxxx', { snd: 'WINSxxx', message: "aaaa" })
    logger.info('WINSTON', { snd: 'WINSTON', message: "aaaa" });

    lg.log('error', 'WINSxxx')
    logger.log('error', 'WINSTON');
});