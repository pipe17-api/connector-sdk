
import * as logform from 'logform';

export type LogLevel = "error" | "warn" | "info" | "debug";

export interface LoggerOptions {
    level?: LogLevel;
    format?: logform.Format;
    meta?: Object;
}

export interface Logger {
    error(message: string, meta?: Object): Logger;
    warn(message: string, meta?: Object): Logger;
    info(message: string, meta?: Object): Logger;
    debug(message: string, meta?: Object): Logger;
    log(level: LogLevel, message: string, meta?: Object): Logger;
    child(meta: Object): Logger
}

export function redactSecrets(info: Object): Object;
export function createLogger(opts?: LoggerOptions): Logger;

export const defaultOpts: LoggerOptions;
export const defaultLogger: Logger;