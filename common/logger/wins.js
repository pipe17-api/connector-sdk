const os = require('os');
const { format } = require('logform')

class Logger {
    constructor(opts) {
        if (typeof (opts) == 'object') {
            this.fmt = opts.format;
            this.level = opts.level;
            this.meta = opts.defaultMeta;
        }
        this.fmt = this.fmt || format.simple();
        this.level = this.level || 'info';
        this.meta = this.meta || { module: 'default' }
        this.levNum = this.toLevNum(this.level);
    }

    filter() {
        if (this.meta.module === 'P17Api') {
            return false;
        }
        return true;
    }

    error(...args) {
        if (!this.filter()) {
            return;
        }
        const msg = this.consMsg("error", args);
        if (console._stderr) {
            console._stderr.write(`${msg}${os.EOL}`)
        } else {
            console.warn(msg)
        }
        return this
    }

    warn(...args) {
        if (!this.filter()) {
            return;
        }
        if (this.levNum < 1) {
            return
        }
        const msg = this.consMsg("warn", args);
        if (console._stderr) {
            console._stderr.write(`${msg}${os.EOL}`)
        } else {
            console.warn(msg)
        }
        return this
    }

    info(...args) {
        if (!this.filter()) {
            return;
        }
        if (this.levNum < 2) {
            return
        }
        const msg = this.consMsg("info", args);
        if (console._stdout) {
            console._stdout.write(`${msg}${os.EOL}`)
        } else {
            console.info(msg)
        }
        return this
    }

    log(level, ...args) {
        if (!this.filter()) {
            return;
        }
        if (level == 'error') {
            this.error(...args);
        } else if (level == 'warn') {
            this.warn(...args);
        } else if (level == 'info') {
            this.info(...args);
        } else if (level == 'debug') {
            this.debug(...args);
        } 
        return this
    }

    debug(...args) {
        if (!this.filter()) {
            return;
        }
        if (this.levNum < 3) {
            return
        }
        const msg = this.consMsg("debug", args);
        if (console._stdout) {
            console._stdout.write(`${msg}${os.EOL}`)
        } else {
            console.info(msg)
        }
        return this
    }

    child(meta) {
        const opts = { level: this.level, format: this.fmt, defaultMeta: { ...this.meta, ...meta } }
        return new Logger(opts)
    }

    consMsg(lev, args) {
        let info;
        if (args.length == 0) {
            info = { ...this.meta, level: lev, message: "" };
        } else if (args.length == 1) {
            const [msg] = args
            info = { ...this.meta, level: lev, message: msg };
        } else if (args.length > 1) {
            let [msg, ext] = args

            if (ext && typeof (ext) == 'object') {
                if ('message' in ext) {
                    msg = `${msg} ${ext.message}`
                    ext = { ...ext }
                    delete ext.message
                }
                info = { ...this.meta, level: lev, message: msg, ...ext };
            } else {
                info = { ...this.meta, level: lev, message: msg };
            }
        }

        let msg = this.fmt.transform(info)
        return msg[Symbol.for('message')]
    }

    toLevNum(level) {
        if (level == 'error') {
            return 0;
        }
        if (level == 'warn') {
            return 1;
        }
        if (level == 'info') {
            return 2;
        }
        if (level == 'debug') {
            return 3;
        }
        return 4;
    }
}

module.exports = {
    Logger
}