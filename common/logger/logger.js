const redact = require('redact-secrets')
const { format } = require('logform')
const { Logger } = require('./wins')

const redactSecrets = (info) => {
  Object.getOwnPropertyNames(info).forEach((key) => {
    info[key] = redact('******').map(info[key])
  }, {});
  return info;
};

const defaultOpts = {
  level: 'info',
  format: format.combine(format.timestamp(), format((info) => {
    return redactSecrets(info);
  })(), format.json()),
  defaultMeta: {
    module: 'default',
  },
};

const createLogger = (opts) => new Logger(opts);

module.exports = {
  defaultOpts,
  redactSecrets,
  createLogger,
  defaultLogger: createLogger(defaultOpts)
}