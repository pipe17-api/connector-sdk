const fetch = require('node-fetch');
const logger = require('../logger').defaultLogger.child({ module: 'converter' });

class ConverterError extends Error {
    constructor(message, status, requestId, text) {
        super(message);
        this.name = "ConverterError";
        this.status = status || 0;
        this.text = text || '';
        this.requestId = requestId || '';
    }
}
exports.ConverterError = ConverterError;

class Converter {
    constructor(config) {
        this.config = config;
        logger.info('ctor', { config });
    }

    async convert(rules, source) {
        logger.info('request', { rules: rules, source: source });

        const response = await fetch.default(this.config.url, {
            method: 'POST',
            headers: {
                'x-api-key': this.config.token,
                'content-type': 'application/json',
                'debug': '' + !!this.config.debug,
            },
            body: JSON.stringify({ rules: rules, source: source })
        });

        if (response.status !== 200) {
            const t = await response.text();
            throw new ConverterError('unexpected_response_status', response.status, '', t);
        }

        const jso = await response.json();
        if (typeof (jso.result) !== 'object') {
            throw new ConverterError(`unexpected_response_body`);
        }

        if (jso.result.status !== 200) {
            throw new ConverterError(`unexpected_result_status`, jso.result.status);
        }

        if (typeof (jso.result.body) === 'undefined') {
            throw new ConverterError(`result_body_undefined`, jso.result.status);
        }

        logger.info('result', { ...jso.result });
        return jso.result.body;
    }
}
exports.Converter = Converter;
9