const AWS = require("aws-sdk");
const logger = require('../logger').defaultLogger.child({ module: 'aws-lambda' });

async function invoke(name, args, endpoint) {
  logger.info('invoke', { name, args });
  endpoint = endpoint || undefined;
  const lambda = new AWS.Lambda({ endpoint: endpoint, maxRetries: 0, httpOptions: { timeout: 900000 } });
  const result = await lambda.invoke({
    FunctionName: name,
    Payload: JSON.stringify(args)
  }).promise();
  logger.info('invoke', { result, name, args })
  return result;
}

async function invokeAsync(name, args, endpoint) {
  logger.info('invokeAsync', { name, args });
  endpoint = endpoint || undefined;
  const lambda = new AWS.Lambda({ endpoint: endpoint, maxRetries: 0, httpOptions: { timeout: 5000 } });
  const result = await lambda.invoke({
    FunctionName: name,
    InvocationType: 'Event',
    Payload: JSON.stringify(args)
  }).promise();
  logger.info('invokeAsync', { result, name, args })
  return result;
}

module.exports = {
  invoke,
  invokeAsync
}