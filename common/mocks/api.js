const fetch = require('node-fetch').default

class APIBase {
  constructor(url, entity, apiKey) {
    this.url = url + '/' + entity;
    this.apiKey = apiKey;
  }
  async exec(path, method, payload) {
    const url = this.url + path;
    const headers = {
      'content-type': 'application/json',
      'x-api-key': this.apiKey,
    }
    const body = payload ? JSON.stringify(payload) : undefined
    const rsp = await fetch(url, { method: method, headers: headers, body: body});
    if (!rsp.ok) {
      if (rsp.status === 404) {
        return null;
      }
      throw new Error(rsp.statusText);
    }
    const jso = await rsp.json();
    return jso;
  }
  async create(o) {
    return await this.exec('', 'POST', o);
  }
  async fetch(id) {
    return await this.exec('/' + id, 'GET');
  }
  async update(id, o) {
    return await this.exec('/' + id, 'PUT', o);
  }
  async remove(id) {
    return await this.exec('/' + id, 'DELETE');
  }
  async list(since) {
    return await this.exec('?since=' + since, 'GET');
  }
}

class OrdersAPI extends APIBase {
  constructor(url, apikey) {
    super(url, 'orders', apikey)
  }
}
class ShipmentsAPI extends APIBase {
  constructor(url, apikey) {
    super(url, 'shipments', apikey)
  }
}

module.exports = {
  APIBase,
  OrdersAPI,
  ShipmentsAPI
}
