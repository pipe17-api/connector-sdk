const faker = require('faker');
const dynamoDB = require('../aws').dynamoDB;

const TableName = process.env.P17SDK_MOCK_SHIPMENTS_TABLE_NAME;
const TableKey = process.env.P17SDK_MOCK_SHIPMENTS_TABLE_KEY_NAME;
const TableSort = process.env.P17SDK_MOCK_SHIPMENTS_TABLE_SORT_NAME;

const generate = () => {
  return {
    [TableKey] : faker.random.alphaNumeric(10),
    "status": "New",
    "line_items" : 
      [...Array(faker.random.number({ min:1, max:10})).keys()].map(() => {
        return {
          "name": faker.commerce.productName(),
          "sku": faker.finance.bic(), 
          "quantity": faker.random.number({ min:1, max:10}), 
          "upc": '' + faker.random.number({ min:100000000000, max:999999999999}), 
        }
      }),
    "carrier": "UPS",
    "note": faker.lorem.sentence(faker.random.number({ min:5, max:10})),
    "address": {
      "firstName": faker.name.firstName(),
      "lastName": faker.name.lastName(),
      "address": faker.address.streetAddress(),
      "address2": faker.address.secondaryAddress(),
      "city": faker.address.city(),
      "state": faker.address.state(),
      "zip": faker.address.zipCode(),
      "country": faker.address.countryCode(),
      "phone": faker.phone.phoneNumber(),
    },
    "valid": "true",
  };
}

const insert = dynamoDB.insert.bind(null, TableName, TableKey);

const fetch = dynamoDB.fetch.bind(null, TableName, TableKey);

const remove = dynamoDB.remove.bind(null, TableName, TableKey);

const update = dynamoDB.update.bind(null, TableName, TableKey);

const last = dynamoDB.last.bind(null, TableName, TableSort);

module.exports = {  
  generate,
  insert,
  fetch,
  remove,
  update,
  last
}
