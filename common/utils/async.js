//export async function sequence<T, U>(items: Array<T>, handler: (item: T) => Promise<U>): Promise<Array<U>> {
async function sequence(items, handler) {  
  const result = [];
  return await items.reduce(async (dfd, item) => {
    await dfd;
    result.push(await handler(item));
    return result;
  }, Promise.resolve(result));
}

async function concurrent(items, handler) {
  return await Promise.all(items.map(handler));
}

module.exports = {
  sequence,
  concurrent,
}