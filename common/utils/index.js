const random = require('./random')
const async = require('./async')

module.exports = {
  random,
  async,
}